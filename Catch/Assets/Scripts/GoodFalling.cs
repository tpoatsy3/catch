﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoodFalling : Falling {

	public Rigidbody good;
	public float radius = 3f;
	public int spawnFrequency = 100;
	public float initialYVelocity = 0f;

//	// Use this for initialization
//	void Start () {
//		
//	}
	
	// Update is called once per frame
	void Update () {
		if (Time.frameCount % spawnFrequency == 0 ) {
			DropBall();
		}
	}

	void DropBall() {
		float y = (float) (transform.position.y - 5);

		float theta = Random.Range(0, Mathf.PI * 2);

		float x = (float) (radius * Mathf.Cos(theta));
		float z = (float) (radius * Mathf.Sin(theta));

		Vector3 pos = new Vector3(x, y, z);

		Rigidbody goodClone = (Rigidbody) Instantiate(good, pos, transform.rotation);
		goodClone.velocity = new Vector3(0, initialYVelocity, 0);
	}
}
